# workflows

I used [draw.io](https://draw.io) to generate this.
I have to provide the raw export if modifications are done on this workflow.

## Summary

[SMS Workflow](###SMS)

## Workflows

### SMS

![sms-workflow.png](./sms-workflow.png "SMS Workflow")
